#TBN Shoutbox Upgrade

## User Tracker
Now you can track users when they are online
![alt text](http://image.prntscr.com/image/6b15c3f8856f4e1590236252b7bb5631.png "Tracker")

##Image Request
Want me to add a new image? Edit and send me the following code.

```
#!html

<li class="hipchat-img" style="display: inline-block;" data-text="">
<img src="URL-GOES-HERE" title="(IMAGE-TITLE-GOES-HERE)" alt="(IMAGE-TITLE-GOES-HERE)" data-smilie="yes">
</li>
```

Only edit the following: URL-GOES-HERE, IMAGE-TITLE-GOES-HERE

#Features  
## User Reply
![alt text](http://i.imgur.com/aMKUEGc.png "User Reply")

## New Smilies
![alt text](http://i.imgur.com/wCQISMc.jpg "New Smilies")

## Requirements
Tampermonkey for Google Chrome or Greasemonkey for Mozilla Firefox.

## Install
Make a new script for Tampermonkey/Greasemonkey and paste in the source code.

Click the tampermokey icon and create a new script  
![alt text](https://i.imgur.com/dfVBDau.png "")

Remove the default code  
![alt text](https://i.imgur.com/RNhN7S6.png "")

From bitbucket copy the source code from shoutbox.js and paste it here  
![alt text](https://i.imgur.com/ph0topT.png "")